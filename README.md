# springCloud

#### 介绍
初步学习springCloud项目 

#### 软件架构
软件架构说明
Spring Cloud 是一系列框架的有序集合，它利用 Spring Boot 的开发便利性简化了分布式系统的开发，比如服务发现、服务网关、服务路由、链路追踪等。Spring Cloud 并不重复造轮子，而是将市面上开发得比较好的模块集成进去，进行封装，从而减少了各模块的开发成本。换句话说：Spring Cloud 提供了构建分布式系统所需的“全家桶”。


#### 使用说明

1. eurekaserver Spring Cloud Netflix 的 Eureka 组件是服务于发现模块即服务注册中心
2. provide 服务提供者
3. gateway 服务网关
4. openFeign 一个大型的系统由多个微服务模块组成，各模块之间不可避免需要进行通信，一般我们可以通过内部接口调用的形式，服务 A 提供一个接口，服务 B 通过 HTTP 请求调用服务 A 的接口，为了简化开发，Spring Cloud 提供了一个基础组件方便不同服务之间的 HTTP 调用，那就是 OpenFeign。
5. config Spring Cloud Config 组件是一个高可用的分布式配置中心，它支持将配置存放到内存（本地），也支持将其放到 Git 仓库进行统一管理（本课主要探讨和 Git 的融合）

![WAI](image/springcloud架构图.png)
