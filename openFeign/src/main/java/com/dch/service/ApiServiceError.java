package com.dch.service;

import org.springframework.stereotype.Component;


@Component
public class ApiServiceError implements ApiService{

    public String index() {
        return "服务发生故障！";
    }
}
