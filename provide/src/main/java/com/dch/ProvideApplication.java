package com.dch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class ProvideApplication {
    public static void main(String[] args){
        System.out.println("================================================== 开始启动 Config         System.out.println(\"================================================== 开始启动 Config Provide =============================================================\");\n =============================================================");
        System.out.println("请在控制台指定Config Provide");

        SpringApplication.run(ProvideApplication.class,args);

        System.out.println("================================================== Config Provide配置中心服务启动成功 =============================================================");
    }
}
